(* ::Package:: *)

(* Copyright (c) 2013, Pouya Dormiani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 *)
(*
 * Mathematica Metris package.
 *
 * Author: Pouya Dormiani
 *)
 
BeginPackage["Metris`"];

playMetris::usage = 
  "Click on the Metris game so that it has selection focus.
  
  Game controls:
  z : start game.
  \[LeftArrow] : shift left.
  \[Rule] : shift right.
  \[UpArrow] : rotate counter clockwise.
  \[DownArrow] : rotate clockwise.
  x : drop the piece.
  
  Options:
  optionGameSpeed -> integer value between 1 (fast) and 10 (slow) inclusive.";
  
Options[playMetris] = {optionGameSpeed -> 3};
  
Begin["`Private`"];

pieces = {square, leftL, rightL, bar, stradBarL, stradBarR, tee};

pieceQ[x_] := MemberQ[pieces, x]
dataConst[square] ^= { 
	{
     {1, 1},
     {1, 1}
    }
};
colorConst[square] ^= Blue;
dataConst[leftL] ^= {
	{
     {0, 1, 0},
     {0, 1, 0},
     {1, 1, 0}
    }, {
     {0, 0, 0},
     {1, 0, 0},
     {1, 1, 1}
    }, {
     {0, 1, 1},
     {0, 1, 0},
     {0, 1, 0}
    }, {
     {0, 0, 0},
     {1, 1, 1},
     {0, 0, 1}
    }
};
colorConst[leftL] ^= Yellow;
dataConst[rightL] ^= {
	{
     {0, 1, 0},
     {0, 1, 0},
     {0, 1, 1}
    }, {
     {0, 0, 0},
     {1, 1, 1},
     {1, 0, 0}
    }, {
     {1, 1, 0},
     {0, 1, 0},
     {0, 1, 0}
    }, {
     {0, 0, 0},
     {0, 0, 1},
     {1, 1, 1}
    }
};
colorConst[rightL] ^= Magenta;
dataConst[bar] ^= {
	{
     {0, 0, 1, 0},
     {0, 0, 1, 0},
     {0, 0, 1, 0},
     {0, 0, 1, 0}
    }, {
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {1, 1, 1, 1}
    }
};
colorConst[bar] ^= Red;
dataConst[stradBarL] ^= {
	{
     {0, 1, 0},
     {1, 1, 0},
     {1, 0, 0}
    }, {
     {0, 0, 0},
     {1, 1, 0},
     {0, 1, 1}
    }
};
colorConst[stradBarL] ^= Orange;
dataConst[stradBarR] ^= {
	{
     {1, 0, 0},
     {1, 1, 0},
     {0, 1, 0}
    }, {
     {0, 0, 0},
     {0, 1, 1},
     {1, 1, 0}
    }
};
colorConst[stradBarR] ^= Green;
dataConst[tee] ^= {
	{
     {0, 0, 0},
     {0, 1, 0},
     {1, 1, 1}
    }, {
     {0, 1, 0},
     {0, 1, 1},
     {0, 1, 0}
    }, {
     {0, 0, 0},
     {1, 1, 1},
     {0, 1, 0}
    }, {
     {0, 1, 0},
     {1, 1, 0},
     {0, 1, 0}
    }
};
colorConst[tee] ^= Purple;

maxDimPieces = Max[Dimensions /@ dataConst /@ pieces];

Module[ {type, pos, data},
    pos[_] := {0, 0};
    piece /: Dot[piece[tag_], f_, r__] := Dot[Evaluate[piece[tag].f], r];
    piece /: new[piece[typeArg_?pieceQ]] := Block[
    	{tag = Unique[]},
        type[tag] = typeArg;
        pos[tag] = pos[tag];
        data[tag] = dataConst[typeArg];
        piece[tag]
    ];
    piece /: free[piece[tag_]] := (
        Unset[type[tag]];
        Unset[pos[tag]];
        Unset[data[tag]];
        Remove[tag]
    );
    piece /: piece[tag_].getType[] := type[tag];
    piece /: piece[tag_].getPosition[] := (pos[tag] = pos[tag]);
    piece /: piece[tag_].setPosition[newPosition : {_Integer, _Integer}] := 
        (pos[tag] = newPosition; piece[tag]);
    piece /: piece[tag_].getColor[] := colorConst[piece[tag].getType[]];
    piece /: piece[tag_].getData[] := First[data[tag]];
    piece /: piece[tag_].getData$[] := data[tag];
    piece /: piece[tag_].getOccupiedIndicies[] := Position[piece[tag].getData[], 1];
    piece /: piece[tag_].getOccupiedIndiciesWithPosition[] := Map[
        Function[dataIdx, piece[tag].getPosition[] + dataIdx - 1], 
        piece[tag].getOccupiedIndicies[]
    ];
    piece /: piece[tag_].setData$[newData_] := (data[tag] = newData; canvas[tag]);
    piece /: piece[tag_].getPositionX[] := Last[piece[tag].getPosition[]];
    piece /: piece[tag_].getPositionY[] := First[piece[tag].getPosition[]];
    piece /: piece[tag_].dataDimensions[] := Dimensions[piece[tag].getData[]];
    piece /: piece[tag_].dataDimensionX[] := Last[piece[tag].dataDimensions[]];
    piece /: piece[tag_].dataDimensionY[] := First[piece[tag].dataDimensions[]];
    piece /: piece[tag_].canvasDimensions := Reverse[piece[tag].dataDimensions[]];
    piece /: piece[tag_].initPosition[c_canvas] := With[{self = piece[tag]},
        self.setPosition[
            {1 + maxDimPieces - self.dataDimensionY[],
            Ceiling[(c.dataDimensionX[] - self.dataDimensionX[])/2]}
		];
        piece[tag]
    ];
    piece /: piece[tag_].rotCClock[] := (
        piece[tag].setData$[RotateLeft[piece[tag].getData$[]]]; piece[tag]);
    piece /: piece[tag_].rotClock[] := (
        piece[tag].setData$[RotateRight[piece[tag].getData$[]]]; piece[tag]);
    piece /: piece[tag_].moveDown[unit_: 1] := With[{self = piece[tag]},
        self.setPosition[{self.getPositionY[] + unit, self.getPositionX[]}];
        self
    ];
    piece /: piece[tag_].moveLeft[unit_: 1] := With[{self = piece[tag]},
        self.setPosition[{self.getPositionY[], self.getPositionX[] + unit}];
        self
    ];
    piece /: piece[tag_].moveRight[unit_: 1] := With[{self = piece[tag]},
        self.setPosition[{self.getPositionY[], self.getPositionX[] - unit}];
        self
    ];
] (* Module: piece *)

Module[ {data},
    canvas /: canvas.dataPadding = 2;
    data[_] := Block[{
        dimY = 22 + maxDimPieces, (* 22 visible vertically, four hidden positions at the top *) 
        dimX = 10, (* 10 visible horizontally *) 
        d},
        d = ConstantArray[0, {dimY + canvas.dataPadding, 10 + 2*canvas.dataPadding}];
        Do[
            d[[;; , j]] = Black;
            d[[;; , dimX + canvas.dataPadding + j]] = Black;
            d[[dimY + j, ;;]] = Black,
        {j, canvas.dataPadding}];
        d
    ];
    canvas /: Dot[canvas[tag_], f_, r__] := Dot[Evaluate[canvas[tag].f], r];
    canvas /: new[canvas[]] := canvas[Unique[]];
    canvas /: free[canvas[tag_]] := (Unset[data[tag]]; Remove[tag]);
    canvas /: canvas[tag_].getData[] := (data[tag] = data[tag]);
    canvas /: canvas[tag_].setData[newData_] := (data[tag] = newData; canvas[tag]);
    canvas /: canvas[tag_].dataDimensions[] := Dimensions[data[tag]];
    canvas /: canvas[tag_].canvasDimensions[] := Reverse[canvas[tag].dataDimensions[]];
    canvas /: canvas[tag_].dataDimensionX[] := Last[canvas[tag].dataDimensions[]];
    canvas /: canvas[tag_].dataDimensionY[] := First[canvas[tag].dataDimensions[]];
    canvas /: canvas[tag_].matrixIdxToCanvasCoord$[{idxY_, idxX_}] := {idxX, idxY} - 1;
    canvas /: canvas[tag_].canvasDataToColorIdxList$[] := 
    With[{self = canvas[tag]},
        With[{ccIdx = Position[self.getData[], Except[0], {2}, Heads -> False]},
            With[{colors = Extract[self.getData[], ccIdx]},
            	Partition[Riffle[colors, ccIdx], 2]
            ]
        ]
    ];
    canvas /: canvas[tag_].draw[p_piece] := With[{self = canvas[tag]},
        {
			EdgeForm[Directive[Thin]],
			p.getColor[],
			(* zero-index the rectangle position, to add to the piece position *)
			Function[idx,       
			    Rectangle[
			    self.matrixIdxToCanvasCoord$[p.getPosition[] + (idx - 1)]]
			] /@ Position[p.getData[], 1]
		}
    ];
    canvas /: canvas[tag_].draw[] := With[{self = canvas[tag]},
		{
			EdgeForm[Directive[Thin]],
			(* Draw canvas contents *)
			Function[{color, idx},
			   {color, Rectangle[self.matrixIdxToCanvasCoord$[idx]]}
			] @@@ (self.canvasDataToColorIdxList$[]),
			(* Draw the canvas header, which hides the piece at its
			 * initial position at the top.*)      
		    Black,
			EdgeForm[],
			Rectangle[
			   self.matrixIdxToCanvasCoord$[{ 1, 1}],
			   self.matrixIdxToCanvasCoord$[
			     { 1 + maxDimPieces, 1 + self.dataDimensionX[]}
			   ]
   	        ]
		}
	];
    canvas /: canvas[tag_].collisionQ[p_piece] := With[
    	{cElements = Extract[canvas[tag].getData[], p.getOccupiedIndiciesWithPosition[]]},
        MemberQ[cElements, Except[0]]
    ];
    canvas /: canvas[tag_].stickToCanvas[p_piece] := (
        canvas[tag].setData[
	      	ReplacePart[
	            canvas[tag].getData[], 
	            Rule[p.getOccupiedIndiciesWithPosition[], p.getColor[]]
	        ]
	    ];
        canvas[tag]
    );
    canvas /: canvas[tag_].emptyRow$[] := Join[
		ConstantArray[Black, canvas.dataPadding],
		ConstantArray[0, canvas[tag].dataDimensionX[] - 2*canvas.dataPadding],
		ConstantArray[Black, canvas.dataPadding]
	];
    canvas /: canvas[tag_].dropRowIdx$[rowIdx_] := With[{self = canvas[tag]},
		self.setData[Prepend[Drop[self.getData[], {rowIdx}], self.emptyRow$[]]]
	];
    canvas /: canvas[tag_].dropFullRows[] := Map[
		canvas[tag].dropRowIdx$[#] &,
		Flatten@Position[
		    (canvas[tag].getData[])[[1 ;; -canvas.dataPadding - 1]],
		    row_List /; Not[MemberQ[row, 0]],
		    {1},
		    Heads -> False
		]
    ];
] (* Module: canvas *)

Module[ {gCanvas, gPiece, gameOver},
    gameEvents = {
	   eventMoveDown, 
	   eventDropPiece,
	   eventMoveLeft,
	   eventMoveRight,
	   eventRotCClock,
	   eventRotClock
    };
    gameEventQ[ge_] := MemberQ[gameEvents, ge];
    (* Allow chaining on gameState objects *)
    gameState /: Dot[gameState[tag_], f_, r__] := Dot[Evaluate[gameState[tag].f], r];
    gameState /: new[gameState[]] := Block[{tag = Unique[]},
	    gCanvas[tag] = new[canvas[]];
        gPiece[tag] = new[piece[RandomChoice[pieces]]].initPosition[gCanvas[tag]];
        gameOver[tag] = False;
        gameState[tag]
    ];
    gameState /: free[gameState[tag_]] := (
        Unset[gCanvas[tag]];
        Unset[gPiece[tag]];
        Remove[tag]
    );
    gameState /: gameState[tag_].getPiece[] := gPiece[tag];
    gameState /: gameState[tag_].setPiece[newPiece_piece] := (gPiece[tag] = newPiece);
    gameState /: gameState[tag_].getCanvas[] := gCanvas[tag];
    gameState /: gameState[tag_].gameOverQ[] := gameOver[tag];
    gameState /: gameState[tag_].setGameOver[] := (gameOver[tag] = True);
    gameState /: gameState[tag_].draw[] := With[
	    {p = gameState[tag].getPiece[], c = gameState[tag].getCanvas[]},
        {c.draw[p], c.draw[]}
    ];
    gameState /: gameState[tag_].drawOptions[] := With[
	    {border = 0.1, c = gameState[tag].getCanvas[]},
        PlotRange -> {{canvas.dataPadding - border, 
                       c.dataDimensionX[] - canvas.dataPadding + border},
                      {maxDimPieces - border, 
                       c.dataDimensionY[] - canvas.dataPadding + border}}
    ];
    gameState /: gameState[tag_].nextGameState[ge_?gameEventQ] := Module[
	    {self = gameState[tag], p, c, pOldPosition, pOldPositionY},
        (* Stop reacting to events when the game is over *)
        If[ self.gameOverQ[], Return[self]];
        (* Process game event *)
        c = self.getCanvas[];
        p = self.getPiece[];
        pOldPosition = p.getPosition[];
        pOldPositionY = p.getPositionY[];
        Switch[ge,
            eventMoveDown, p.moveDown[],
            eventMoveLeft, p.moveLeft[],
            eventMoveRight, p.moveRight[],
            eventRotCClock, p.rotCClock[],
            eventRotClock, p.rotClock[],
            eventDropPiece, While[!(c.collisionQ[p]),
								pOldPosition = p.getPosition[];
								pOldPositionY = p.getPositionY[];
								p.moveDown[];
                            ]
        ];
        If[c.collisionQ[p],
    	   Switch[ge,
               (* A collision occured on moveDown, stick the piece, clear rows *)
               eventMoveDown | eventDropPiece,
               p.setPosition[pOldPosition];
               If[p.getPositionY[] <= maxDimPieces,
                   self.setGameOver[];
                   Throw[self, gameOverTag]
               ];
               c.stickToCanvas[p];
               free[p];
               self.setPiece[new[piece[RandomChoice[pieces]]].initPosition[c]];
               c.dropFullRows[],
               (**)
               (eventMoveLeft | eventMoveRight),
               p.setPosition[pOldPosition],
               (**)
               eventRotCClock,
               p.rotClock[],
               (**)
               eventRotClock,
               p.rotCClock[]
           ]
       ];
       self
   ];
   
] (* Module: gameState *)

Options[playMetris$] = Options[playMetris];
playMetris$[OptionsPattern[]] := DynamicModule[{gs = new[gameState[]]},
    cleanUpAndAbort[gs_gameState, gameOverTag] := StopScheduledTask[dropTask];
    dropTask = CreateScheduledTask[
	   Catch[gs.nextGameState[eventMoveDown], gameOverTag, cleanUpAndAbort],
       Min[Max[1, OptionValue[optionGameSpeed]], 10]/10
    ];
    EventHandler[
        Rotate[Graphics[Dynamic[gs.draw[]], gs.drawOptions[]], 180 Degree],
    	{"LeftArrowKeyDown"  :> gs.nextGameState[eventMoveLeft],
         "RightArrowKeyDown" :> gs.nextGameState[eventMoveRight],
         "DownArrowKeyDown"  :> gs.nextGameState[eventRotClock],
         "UpArrowKeyDown"    :> gs.nextGameState[eventRotCClock],
         {"KeyDown", "x"}    :> Catch[gs.nextGameState[eventDropPiece],gameOverTag,cleanUpAndAbort],
     	 {"KeyDown", "z"}    :> StartScheduledTask[dropTask]
        }
    ]
];
  
playMetris[options: OptionsPattern[]]:=playMetris$[options];

End[];

EndPackage[];

